FROM tensorflow/serving
COPY /NeuralNetwork /models/neuralnetwork
ENV MODEL_NAME=neuralnetwork
EXPOSE 8501