# machine_error_detection

Train a Neural Network to detect Machine Errors

![The Machine](machine.jpg)

The Machine is a Lego NXT Segway and we assume the following states:

| Machine       | status        |
| ------------- |:-------------:|
| balancing     | working 		|
| tipped over   | not working   |

The sensor attached to the machine is a Tinkerforge IMU 2.0 (x-front, y-up like on the picture) and we collected some training data for the states.

## Pretrained Model

The model is saved in the `/NeuralNetwork` directory and you can run it with a TensorFlow Serving Container:

```
docker run -it --rm -p 8501:8501 \
    -v "$(pwd)/NeuralNetwork:/models/neuralnetwork" \
    -e MODEL_NAME=neuralnetwork \
    tensorflow/serving
```

Now visit http://localhost:8501/v1/models/neuralnetwork

## Build and Deploy as Container

To build and push it to the official Docker Hub Registry:

    docker build -t mechlabengineering/machine_error_detection
    docker push mechlabengineering/machine_error_detection

You'll find the Container [there](https://hub.docker.com/r/mechlabengineering/machine_error_detection).

Now you can set up a Amazon [Elastic Container Service (ECS)](https://eu-central-1.console.aws.amazon.com/ecs/home?region=eu-central-1#/clusters) with the model.

## Example App

There is an example Browser App (based on the [Tinkerforge IMU 2.0 Example](https://www.tinkerforge.com/de/doc/Software/Bricks/IMUV2_Brick_JavaScript.html#javascript-imu-brick-2-0)) in the /App folder, which will receive the IMU data via websocket and POST it to the Tensorflow Serving API and visualize the predicted machine state.

You need the [Tinkerforge Brick Deamon (brickd)](https://www.tinkerforge.com/en/doc/Software/Brickd.html) installed with enabled Websocket and your Browser probably needs disabled CORS to access localhost via REST API.