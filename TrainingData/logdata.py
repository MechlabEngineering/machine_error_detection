#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tinkerforge.brick_imu_v2 import BrickIMUV2
from tinkerforge.ip_connection import IPConnection
import time

HOST = "localhost"
PORT = 4223
UID = "5Xaeob"  # UID of your IMU Brick 2.0


LOGFILE = './data.csv'

with open(LOGFILE, 'w', encoding='utf-8') as logfile:
    logfile.write(
        'timestamp,ax [m/s²],ay [m/s²],az [m/s²],rx [°/s],ry [°/s],rz [°/s],mx [µT],my [µT],mz [µT],y [°],r [°],p [°]\n')


def cb_all_data(acceleration, magnetic_field, angular_velocity, euler_angle, quaternion,
                linear_acceleration, gravity_vector, temperature, calibration_status):

    with open(LOGFILE, 'a', encoding='utf-8') as logfile:
        logfile.write('%s,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\n' % (
            str(time.time()),
            acceleration[0]/100.0,
            acceleration[1]/100.0,
            acceleration[2]/100.0,
            angular_velocity[0]/16.0,
            angular_velocity[1]/16.0,
            angular_velocity[2]/16.0,
            magnetic_field[0]/16.0,
            magnetic_field[1]/16.0,
            magnetic_field[2]/16.0,
            euler_angle[0]/16.0,
            euler_angle[1]/16.0,
            euler_angle[2]/16.0
        )
        )


if __name__ == "__main__":
    ipcon = IPConnection()  # Create IP connection
    imu = BrickIMUV2(UID, ipcon)  # Create device object

    ipcon.connect(HOST, PORT)  # Connect to brickd
    # Don't use device before ipcon is connected

    # Register all data callback to function cb_all_data
    imu.register_callback(imu.CALLBACK_ALL_DATA, cb_all_data)

    # Set period for all data callback to 0.1s (100ms)
    imu.set_all_data_period(100)

    input("Writing to \'%s\'. Press key to exit\n" % LOGFILE)
    ipcon.disconnect()
